import csv
import logging
import logging.config
import globVar
import datetime
import os
import shutil
import json
import xml.etree.ElementTree as ET
import gzip
import random
import csv
import commands

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("monitor")
except Exception, e:
    print(e)

WORK_DIR = globVar.WORK_DIR
FACTORS_FILE = globVar.FACTORS_FILE

now = datetime.datetime.now()

def sumList(list, factor=1.000):
    sum = 0
    for ele in list:
        sum = sum + int(ele * factor)
    return sum
def writeRateCsv(csvfile, date, time, tac, SuccEpsAttachRate, PagingSuccRate, TauUpdateRate):
    csvLine = [date, time, tac, SuccEpsAttachRate, PagingSuccRate, TauUpdateRate]
    if not os.path.isfile(csvfile):
        f = open(csvfile, "w")
        f.close()
        csvFile = file(csvfile, "ab")
        writter = csv.writer(csvFile)
        writter.writerow(["date", "time+15", "tac", "SuccEpsAttachRate", "PagingSuccRate", "TauUpdateRate"])
        csvFile.close()
    csvFile = file(csvfile, "ab")
    writter = csv.writer(csvFile)
    writter.writerow(csvLine)
    csvFile.close()
def writeCsv(csvfile, date, time, tac, mme, counterName, value):
    csvLine = [date, time, tac, mme, counterName, value]
    if not os.path.isfile(csvfile):
        f = open(csvfile, "w")
        f.close()
        csvFile = file(csvfile, "ab")
        writter = csv.writer(csvFile)
        writter.writerow(["date", "time+15", "tac", "mme", "counterName", "value"])
        csvFile.close()
    csvFile = file(csvfile, "ab")
    writter = csv.writer(csvFile)
    writter.writerow(csvLine)
    csvFile.close()
def scan():
    PERIOD = -1
    i = 0
    while i > PERIOD:
        processedList = []  # Read from <date>.processed.json
        filePathList = []
        pmFilesNamelist = []
        delta = datetime.timedelta(hours=i)
        folderTime = (now + delta).strftime("%Y%m%d%H")
        dir = os.path.join(globVar.WORK_DIR, folderTime)  # locate to different input folder by input/YYMMDDhh

        logger.info("checking {dir}".format(dir=dir))
        cmd = "ls -th {dir} |  egrep \"old|newmon\"".format(dir=dir)  # find all PM files under input/YYMMDDhh
        status, output = commands.getstatusoutput(cmd)
        logger.info(cmd)
        if status == 0:
            # Get all MME PM files name list - pmFilesNamelist
            pmFilesNamelist = output.split("\n")
            logger.info(pmFilesNamelist)
            #logger.info("found total {num} MME PM files".format(num=len(pmFilesNamelist)))
            processedList = processedList + pmFilesNamelist
        else:
            logger.warning("code: {status}, output: {output}".format(status=status, output=output))
        i = i - 1

    for fileName in pmFilesNamelist:
        fileDirTime = fileName.split("-")[4][:10]   # 019061022 in /PM-MME-A1-V3.0.0-20190610225200-15.xml.gz
        workDir = os.path.join(globVar.WORK_DIR, fileDirTime)
        workFile = os.path.join(workDir, fileName)
        filePathList.append(workFile)
    logger.info("found total {num} MME PM files: {files}".format(num=len(pmFilesNamelist), files=filePathList))
    return filePathList

def monMME():
    logger.info("START")
    filePathList = scan()   # append all MME file full path

    # Load factors.json
    factorsDict = {}
    FACTORS_FILE = globVar.FACTORS_FILE
    with open(FACTORS_FILE, "r") as f:
        factorsDict = json.load(f)
    #logger.debug("factors list: {f}".format(f=factorsDict))
    taList = factorsDict.keys()
    #logger.debug("TA list: {ta}".format(ta=taList))

    # counters for compute SuccEpsAttachRate, PagingSuccRate, TauUpdateRate
    countersForComputeList = ["MM.SuccEpsAttach._Ta", "MM.AttEpsAttach._Ta", "MM.FailedEpsAttach._Ta.7.User",
                              "MM.FailedEpsAttach._Ta.15.User", \
                              "MM.FailedEpsAttach._Ta.19.User", "MM.FirstPagingSucc._Ta", "MM.SecondPagingSucc._Ta",
                              "MM.PagAtt._Ta", \
                              "MM.TauAccept._Ta", "MM.TauRequest._Ta"]

    # counter name in factor.json mapping to CV N name
    counterCVNFirstHalfMappingDict = {"MM.SuccEpsAttach._Ta": "MM.SuccEpsAttach.", \
                                      "MM.AttEpsAttach._Ta": "MM.AttEpsAttach.", \
                                      "MM.FailedEpsAttach._Ta.7.User": "MM.FailedEpsAttach.", \
                                      "MM.FailedEpsAttach._Ta.15.User": "MM.FailedEpsAttach.", \
                                      "MM.FailedEpsAttach._Ta.19.User": "MM.FailedEpsAttach.", \
                                      "MM.FirstPagingSucc._Ta": "MM.FirstPagingSucc.", \
                                      "MM.SecondPagingSucc._Ta": "MM.SecondPagingSucc.", \
                                      "MM.PagAtt._Ta": "MM.PagAtt.", \
                                      "MM.TauAccept._Ta": "MM.TauAccept.", \
                                      "MM.TauRequest._Ta": "MM.TauRequest.", \
                                      }

    # Process
    for file in filePathList:
        logger.info("start processing {file}".format(file=file))
        fileName = os.path.basename(file)
        # PM-MME-A1-V3.0.0-20190610225200-15.xml.old
        csvDay = fileName.split("-")[4][:8]   # 20190610
        csvTime = fileName.split("-")[4][8:10] + ":" + fileName.split("-")[4][10:12]   # 22:52
        # start process
        """
        # data var for recording whole detai counter data
        Example
        data = {
            "MM.SuccEpsAttach.4600009052": [1,2,3,4,5,6],
            "MM.FailedEpsAttach.4600009052": [2,3,4,5,6,7,8,9],
        }
        """
        data = {}

        tree = ET.parse(file)
        root = tree.getroot()
        #logger.info("parsed {file}".format(file=file))

        for ta in taList:
            #logger.info("TA: {ta}".format(ta=ta))
            counters = factorsDict[ta][0].keys()
            counterIndexMappingDict = {}

            # Get Counter index number
            for N in root.getiterator("N"):
                if N.text in counters or N.text in countersForComputeList:
                    index = N.attrib["i"]
                    counterIndexMappingDict[N.text] = index
                    #logger.info("found counter: {counter}, index: {index}".format(counter=N.text, index=index))
            #logger.info("counter index mapping: {map}".format(map=counterIndexMappingDict))

            for i in range(len(root)):
                Measurements = root[i]
                for j in range(len(Measurements)):
                    if root[i][j].tag == "ObjectType" and root[i][j].text == "MmeFunction":
                        MeasurementsLoc = i
                        #logger.info("found ObjectType: {ot}, location: {loc}".format(ot=root[i][j].text, loc=MeasurementsLoc))
                        break
            for i in range(len(root[MeasurementsLoc])):
                if root[MeasurementsLoc][i].tag == "PmData":
                    PmDataLoc = i
                    #logger.info("found PmData location: {loc}".format(loc=PmDataLoc))
                    break
            ObjectNum = len(root[MeasurementsLoc][PmDataLoc])
            logger.info("found total MME: {num}".format(num=ObjectNum))

            # generate data {}
            countersForComputeSumDict = {}
            if not ta in countersForComputeSumDict.keys():
                countersForComputeSumDict[ta] = {}
            for counter in countersForComputeList:
                if counter not in countersForComputeSumDict[ta].keys():
                    countersForComputeSumDict[ta][counter] = 0
                sum = countersForComputeSumDict[ta][counter]
                data[counter] = []
                for i in range(ObjectNum):
                    # Object
                    ObjectLoc = i
                    UserLabel = root[MeasurementsLoc][PmDataLoc][ObjectLoc].get("UserLabel")
                    for j in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc])):
                        if root[MeasurementsLoc][PmDataLoc][ObjectLoc][j].get("i") == counterIndexMappingDict[counter]:
                            CVLoc = j
                            if counter in counterCVNFirstHalfMappingDict:
                                counterWithTAName = counterCVNFirstHalfMappingDict[counter] + ta
                                for k in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc])-1):
                                    SNSVLoc = k
                                    SN = root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc].text
                                    if SN == counterWithTAName:
                                        SV = int(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc + 1].text)
                                        data[counter].append(SV)
                                        #logger.debug("{ta},{mme} {counter} {sn} {sv}".format(ta=ta, mme=UserLabel, counter=counter, sn=SN, sv=SV))
                                        if ".old" in fileName:
                                            counterCsvFile = os.path.join(globVar.CSV_DIR, "PM-MME-" + csvDay + ".ori.csv")
                                        elif ".new" in fileName:
                                            counterCsvFile = os.path.join(globVar.CSV_DIR, "PM-MME-" + csvDay + ".upg.csv")
                                        else:
                                            counterCsvFile = os.path.join(globVar.CSV_DIR, "PM-MME-" + csvDay + ".UNKNOWN.csv")
                                        writeCsv(counterCsvFile, csvDay, csvTime, ta, UserLabel, counter, SV)
                                    else:
                                        pass
                            else:
                                logger.error("{counter} Not in CV N First Half Mapping Directory".format(counter=counter))
                        else:
                            pass

            # Caculate Rate
            try:
                # SuccEpsAttachRate
                if (sumList(data["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])) == 0:
                    SuccEpsAttachRate = 0
                    logger.info("{ta} SuccEpsAttachRate: 0, due to {c1}-{c2}-{c3}-{c4} is 0".format(ta=ta, c1="MM.AttEpsAttach._Ta", c2="MM.FailedEpsAttach._Ta.7.User", c3="MM.FailedEpsAttach._Ta.15.User", c4="MM.FailedEpsAttach._Ta.19.User"))
                else:
                    SuccEpsAttachRate = float(sumList(data["MM.SuccEpsAttach._Ta"])) / float((sumList(data["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])))
            except Exception, e:
                logger.error(e)
            try:
                # PagingSuccRate
                if sumList(data["MM.PagAtt._Ta"]) == 0:
                    PagingSuccRate = 0
                    logger.info("{ta} PagingSuccRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.PagAtt._Ta"))
                else:
                    PagingSuccRate = float((sumList(data["MM.FirstPagingSucc._Ta"]) + sumList(data["MM.SecondPagingSucc._Ta"]))) / float(sumList(data["MM.PagAtt._Ta"]))
            except Exception, e:
                logger.error(e)
            try:
                # TauUpdateRate
                if sumList(data["MM.TauRequest._Ta"]) == 0:
                    TauUpdateRate = 0
                    logger.info("{ta} TauUpdateRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.TauRequest._Ta"))
                else:
                    TauUpdateRate = float(sumList(data["MM.TauAccept._Ta"])) / float(sumList(data["MM.TauRequest._Ta"]))
            except Exception, e:
                logger.error(e)

            if ".old" in fileName:
                csvRateFile = os.path.join(globVar.CSV_DIR, "PM-MME-" + csvDay + ".ori.rate.csv")
            elif ".new" in fileName:
                csvRateFile = os.path.join(globVar.CSV_DIR, "PM-MME-" + csvDay + ".upg.rate.csv")
            else:
                csvRateFile = os.path.join(globVar.CSV_DIR, "PM-MME-" + csvDay + ".UNKNOWN.rate.csv")
            writeRateCsv(csvRateFile, csvDay, csvTime, ta, SuccEpsAttachRate, PagingSuccRate, TauUpdateRate)

        os.remove(file)
        logger.info("removed {file}".format(file=file))

    logger.info("END")

if __name__ == "__main__":
    monMME()