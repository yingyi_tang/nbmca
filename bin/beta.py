
import datetime
import globVar
import commands
import os
import logging
import logging.config
import subprocess
import json
import shutil
import xml.etree.ElementTree as ET
import gzip
import random
import csv

SuccEpsAttachRate_Max = 0.995
SuccEpsAttachRate_Min = 0.985

PagingSuccRate_Max = 0.995
PagingSuccRate_Min = 0.985

TauUpdateRate_Max = 0.995
TauUpdateRate_Min = 0.985

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)

now = datetime.datetime.now()

def startBanner():
    logger.info("START")

def checkProcess():
    cmd = "ps -ef | grep alpha.py | grep -v grep"
    status, output = commands.getstatusoutput(cmd)
    ret = output.split("\n")
    if len(ret) > 1:
        logger.warning(ret)
        logger.warning("pqmme still running, exit")
        exit(1)
    else:
        logger.info("check process passed")
        pass

def scanAndCopy():
    """
    Scan the pass PERIOD hours folders under input directory (default is 9 hours)
    MME PM files: copy to repo and wait to be precessed
    Other PM files: copy to northbound directory and append in processList
    :return:
    """
    logger.info("#--- Entering Phase 1: Scan And Copy ---#")
    PERIOD = -1
    i = 0

    while i >= PERIOD:
        processedList = []  # Read from <date>.processed.json
        delta = datetime.timedelta(hours=i)
        folderTime = (now + delta).strftime("%Y%m%d%H")
        dir = os.path.join(globVar.INPUT_DIR, folderTime)   # locate to different input folder by input/YYMMDDhh

        logger.info("checking {dir}".format(dir=dir))
        cmd = "ls -th {dir} | grep PM- | grep gz".format(dir=dir)   # find all PM files under input/YYMMDDhh
        status, output = commands.getstatusoutput(cmd)
        logger.info(cmd)
        if status == 0:
            # Get all PM files name list - pmFilesNamelist
            pmFilesNamelist = output.split("\n")
            logger.info(pmFilesNamelist)
            logger.info("found total {num} pm files".format(num=len(pmFilesNamelist)))

            #  Get Processed PM files name list - processedFile
            processedFile = os.path.join(globVar.LOG_DIR, folderTime+".processed.json")
            if os.path.isfile(processedFile):
                with open(processedFile, "r") as f:
                    processedList = json.load(f)
                logger.info("read processed json file: {file}".format(file = processedFile))
                #logger.info("processed files list: {list}".format(list=processedList))
                newlyAddedFilesList = [ x for x in pmFilesNamelist if x not in processedList ]
                logger.info("newly added {num} files list: {list}".format(num=len(newlyAddedFilesList), list=newlyAddedFilesList))
            else:
                logger.info("processed json file is not exist")
                newlyAddedFilesList = pmFilesNamelist
                logger.info("newly added {num} files list: {list}".format(num=len(newlyAddedFilesList), list=newlyAddedFilesList))

            # Process MME and other PM files
            for file in newlyAddedFilesList:
                # process MME file, JUST copy to repo directory
                if "PM-MME-" in file:
                    shutil.copy(os.path.join(dir, file), os.path.join(globVar.REPO_DIR, file))
                    logger.info("copied {file} to {dir}".format(file=os.path.join(dir, file), dir=os.path.join(globVar.REPO_DIR, file)))
                # prcess non MME file, DIRECTLY copy to northbound direcotory
                else:
                    northboundOutDir = os.path.join(globVar.OUTPUT_DIR, folderTime)
                    if not os.path.isdir(northboundOutDir):
                        os.mkdir(northboundOutDir)
                        logger.info("created {dir}".format(dir=northboundOutDir))
                    shutil.copy(os.path.join(dir, file), os.path.join(northboundOutDir, file))
                    logger.info("copied {file} to {dir}".format(file=os.path.join(dir, file), dir=os.path.join(northboundOutDir, file)))
                    processedList.append(file)

            recodFile = os.path.join(globVar.TMP_DIR, now.strftime("%Y%m%d%H%M") + ".newlyAdded.tmp")
            with open(recodFile, "w") as f:
                json.dump(newlyAddedFilesList, f, indent=1)

            with open(processedFile, "w") as f:
                json.dump(processedList, f, indent=1)
            if len(newlyAddedFilesList) == 0:
                logger.info("no update in processed json {pfile}".format(pfile=processedFile))
            else:
                logger.info("updated {fileName} in processed json {pfile}".format(fileName=newlyAddedFilesList, pfile=processedFile))
        else:
            logger.warning("code: {status}, output: {output}".format(status=status, output=output))

        i = i - 1
    logger.info("#--- Exit Phase 1: Scan And Copy ---#")



def sumList(list, factor=1.000):
    sum = 0
    for ele in list:
        sum = sum + int(ele * factor)
    return sum

def process():
    """
    Process MME files in repo direcotory
    :return:
    """
    logger.info("#--- Entering Phase 2: Process MME files in repo ---#")
    filePathList = []   # append all MME file full path
    # Find all mme files under repo directory
    for _, _, repoFilesName in os.walk(globVar.REPO_DIR):
        logger.info("found mme files list: {list} under {dir}".format(list=repoFilesName,dir=globVar.REPO_DIR))
        #repoFilesName example: ['PM-MME-A1-V3.0.0-20190610225200-15.xml.gz', 'PM-MME-A1-V3.0.0-20190610220700-15.xml.gz']

    # Move mme files to work directory and generate filePathList
    for repoFileName in repoFilesName:
        repoFileNorthboundDirTime = repoFileName.split("-")[4][:10]   # 019061022 in /PM-MME-A1-V3.0.0-20190610225200-15.xml.gz
        workDir = os.path.join(globVar.WORK_DIR, repoFileNorthboundDirTime)
        if not os.path.isdir(workDir):
            os.mkdir(workDir)
            logger.info("create directory {dir}".format(dir=workDir))
        workFile = os.path.join(workDir, repoFileName)
        shutil.move(os.path.join(globVar.REPO_DIR, repoFileName), workFile)   # move MME file from repo to work
        filePathList.append(workFile)
        logger.info("moved {sfile} to {dfile}".format(sfile=os.path.join(globVar.REPO_DIR, repoFileName), dfile=workFile))

    # Load factors.json
    factorsDict = {}
    FACTORS_FILE = globVar.FACTORS_FILE
    with open(FACTORS_FILE, "r") as f:
        factorsDict = json.load(f)
    logger.debug("factors list: {f}".format(f=factorsDict))
    taList = factorsDict.keys()
    logger.debug("TA list: {ta}".format(ta=taList))

    # counters for compute SuccEpsAttachRate, PagingSuccRate, TauUpdateRate
    countersForComputeList = ["MM.SuccEpsAttach._Ta", "MM.AttEpsAttach._Ta", "MM.FailedEpsAttach._Ta.7.User",
                              "MM.FailedEpsAttach._Ta.15.User", \
                              "MM.FailedEpsAttach._Ta.19.User", "MM.FirstPagingSucc._Ta", "MM.SecondPagingSucc._Ta",
                              "MM.PagAtt._Ta", \
                              "MM.TauAccept._Ta", "MM.TauRequest._Ta"]

    # counter name in factor.json mapping to CV N name
    counterCVNFirstHalfMappingDict = {"MM.SuccEpsAttach._Ta": "MM.SuccEpsAttach.", \
                                      "MM.AttEpsAttach._Ta": "MM.AttEpsAttach.", \
                                      "MM.FailedEpsAttach._Ta.7.User": "MM.FailedEpsAttach.", \
                                      "MM.FailedEpsAttach._Ta.15.User": "MM.FailedEpsAttach.", \
                                      "MM.FailedEpsAttach._Ta.19.User": "MM.FailedEpsAttach.", \
                                      "MM.FirstPagingSucc._Ta": "MM.FirstPagingSucc.", \
                                      "MM.SecondPagingSucc._Ta": "MM.SecondPagingSucc.", \
                                      "MM.PagAtt._Ta": "MM.PagAtt.", \
                                      "MM.TauAccept._Ta": "MM.TauAccept.", \
                                      "MM.TauRequest._Ta": "MM.TauRequest.", \
                                      }

    # Process
    for file in filePathList:
        logger.info("start processing {file}".format(file=file))
        fileName = os.path.basename(file)
        # start process
        """
        # data var for recording whole detai counter data
        Example
        data = {
            "MM.SuccEpsAttach.4600009052": [1,2,3,4,5,6],
            "MM.FailedEpsAttach.4600009052": [2,3,4,5,6,7,8,9],
        }
        """
        data = {}

        fh = gzip.open(file, "r")
        try:
            content = fh.read()
        except Exception, e:
            logger.error(e)
        finally:
            fh.close()
        oldFile = file.replace(".gz", ".old")
        with open(oldFile, "w") as f:
            f.write(content)
        logger.info("generated {file}".format(file=oldFile))

        tree = ET.parse(oldFile)
        root = tree.getroot()
        logger.info("parsed {file}".format(file=oldFile))

        for ta in taList:
            logger.info("TA: {ta}".format(ta=ta))
            counters = factorsDict[ta][0].keys()
            counterIndexMappingDict = {}

            # Get Counter index number
            for N in root.getiterator("N"):
                if N.text in counters or N.text in countersForComputeList:
                    index = N.attrib["i"]
                    counterIndexMappingDict[N.text] = index
                    #logger.info("found counter: {counter}, index: {index}".format(counter=N.text, index=index))
            logger.info("counter index mapping: {map}".format(map=counterIndexMappingDict))

            for i in range(len(root)):
                Measurements = root[i]
                for j in range(len(Measurements)):
                    if root[i][j].tag == "ObjectType" and root[i][j].text == "MmeFunction":
                        MeasurementsLoc = i
                        logger.info("found ObjectType: {ot}, location: {loc}".format(ot=root[i][j].text, loc=MeasurementsLoc))
                        break
            for i in range(len(root[MeasurementsLoc])):
                if root[MeasurementsLoc][i].tag == "PmData":
                    PmDataLoc = i
                    logger.info("found PmData location: {loc}".format(loc=PmDataLoc))
                    break
            ObjectNum = len(root[MeasurementsLoc][PmDataLoc])
            logger.info("found total MME: {num}".format(num=ObjectNum))

            # generate data {}
            countersForComputeSumDict = {}
            if not ta in countersForComputeSumDict.keys():
                countersForComputeSumDict[ta] = {}
            for counter in countersForComputeList:
                if counter not in countersForComputeSumDict[ta].keys():
                    countersForComputeSumDict[ta][counter] = 0
                sum = countersForComputeSumDict[ta][counter]
                data[counter] = []
                for i in range(ObjectNum):
                    # Object
                    ObjectLoc = i
                    UserLabel = root[MeasurementsLoc][PmDataLoc][ObjectLoc].get("UserLabel")
                    for j in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc])):
                        if root[MeasurementsLoc][PmDataLoc][ObjectLoc][j].get("i") == counterIndexMappingDict[counter]:
                            CVLoc = j
                            if counter in counterCVNFirstHalfMappingDict:
                                counterWithTAName = counterCVNFirstHalfMappingDict[counter] + ta
                                #logger.info("processing {counter} with TA: {cvn}".format(counter=counter, cvn=counterWithTAName))
                                for k in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc])-1):
                                    SNSVLoc = k
                                    SN = root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc].text
                                    if SN == counterWithTAName:
                                        SV = int(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc + 1].text)
                                        data[counter].append(SV)
                                        logger.debug("{ta},{mme} {counter} {sn} {sv}"\
                                                     .format(ta=ta, mme=UserLabel, counter=counter, sn=SN, sv=SV))
                                         #logger.debug(data)
                                        #logger.info("found {mme} {counter} with value {value}".format(mme=UserLabel, counter=SN, value=SV))
                                    else:
                                        pass
                            else:
                                logger.error("{counter} Not in CV N First Half Mapping Directory".format(counter=counter))
                        else:
                            pass

            # Caculate Original Rate and New Rate mutiply with factor
            try:
                # SuccEpsAttachRate
                if (sumList(data["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])) == 0:
                    SuccEpsAttachRate = 0
                    logger.info("{ta} SuccEpsAttachRate: 0, due to {c1}-{c2}-{c3}-{c4} is 0".format(ta=ta, c1="MM.AttEpsAttach._Ta", c2="MM.FailedEpsAttach._Ta.7.User", c3="MM.FailedEpsAttach._Ta.15.User", c4="MM.FailedEpsAttach._Ta.19.User"))
                    SuccEpsAttachRate_New = 0
                else:
                    SuccEpsAttachRate = float(sumList(data["MM.SuccEpsAttach._Ta"])) / float((sumList(data["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])))
                    SuccEpsAttachRate_New = float(sumList(data["MM.SuccEpsAttach._Ta"], factorsDict[ta][0]["MM.SuccEpsAttach._Ta"])) / float((sumList(data["MM.AttEpsAttach._Ta"], factorsDict[ta][0]["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])))
                    logger.info("{ta} original SuccEpsAttachRate: {orate}, with factors SuccEpsAttachRate: {nrate}".format(ta=ta, orate=SuccEpsAttachRate, nrate=SuccEpsAttachRate_New))
            except Exception, e:
                logger.error(e)
            try:
                # PagingSuccRate
                if sumList(data["MM.PagAtt._Ta"]) == 0:
                    PagingSuccRate = 0
                    PagingSuccRate_New = 0
                    logger.info("{ta} PagingSuccRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.PagAtt._Ta"))
                else:
                    PagingSuccRate = float((sumList(data["MM.FirstPagingSucc._Ta"]) + sumList(data["MM.SecondPagingSucc._Ta"]))) / float(sumList(data["MM.PagAtt._Ta"]))
                    PagingSuccRate_New = float((sumList(data["MM.FirstPagingSucc._Ta"], factorsDict[ta][0]["MM.FirstPagingSucc._Ta"]) + sumList(data["MM.SecondPagingSucc._Ta"]))) / float(sumList(data["MM.PagAtt._Ta"], factorsDict[ta][0]["MM.PagAtt._Ta"]))
                    logger.info("{ta} original PagingSuccRate: {orate}, with factors PagingSuccRate: {nrate}".format(ta=ta, orate=PagingSuccRate, nrate=PagingSuccRate_New))
            except Exception, e:
                logger.error(e)
            try:
                # TauUpdateRate
                if sumList(data["MM.TauRequest._Ta"]) == 0:
                    TauUpdateRate = 0
                    TauUpdateRate_New = 0
                    logger.info("{ta} TauUpdateRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.TauRequest._Ta"))
                else:
                    TauUpdateRate = float(sumList(data["MM.TauAccept._Ta"])) / float(sumList(data["MM.TauRequest._Ta"]))
                    TauUpdateRate_New = float(sumList(data["MM.TauAccept._Ta"], factorsDict[ta][0]["MM.TauAccept._Ta"])) / float(sumList(data["MM.TauRequest._Ta"]))
                    logger.info("{ta} original TauUpdateRate: {orate}, with factors TauUpdateRate: {nrate}".format(ta=ta, orate=TauUpdateRate, nrate=TauUpdateRate_New))
            except Exception, e:
                logger.error(e)

            # Adjust factor
            # SuccEpsAttachRate
            if SuccEpsAttachRate >= 0.985:
                factorsDict[ta][0]["MM.SuccEpsAttach._Ta"] = 1.000
                factorsDict[ta][0]["MM.AttEpsAttach._Ta"] = 1.000
                logger.info("{ta} SuccEpsAttachRate already >= 0.985, set MM.SuccEpsAttach._Ta and MM.AttEpsAttach._Ta factor to 1".format(ta=ta))
                SuccEpsAttachRate_New = SuccEpsAttachRate
            elif SuccEpsAttachRate_New< 0:
                factorsDict[ta][0]["MM.SuccEpsAttach._Ta"] = 1.000
                factorsDict[ta][0]["MM.AttEpsAttach._Ta"] = 1.000
                logger.info("{ta} SuccEpsAttachRate exception, set MM.SuccEpsAttach._Ta and MM.AttEpsAttach._Ta factor to 1".format(ta=ta))
                SuccEpsAttachRate_New = SuccEpsAttachRate
            elif SuccEpsAttachRate_New > 1:
                SuccEpsAttachRate_Target = random.uniform(SuccEpsAttachRate_Min, SuccEpsAttachRate_Max)
                SuccEpsAttachRate_TryTimes = 0
                while(SuccEpsAttachRate_New >= SuccEpsAttachRate_Target):
                    SuccEpsAttachRate_TryTimes = SuccEpsAttachRate_TryTimes + 1
                    factorsDict[ta][0]["MM.AttEpsAttach._Ta"] = factorsDict[ta][0]["MM.AttEpsAttach._Ta"] + 0.001
                    SuccEpsAttachRate_New = float(sumList(data["MM.SuccEpsAttach._Ta"],factorsDict[ta][0]["MM.SuccEpsAttach._Ta"])) / float((sumList(data["MM.AttEpsAttach._Ta"],factorsDict[ta][0]["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])))
                logger.info("{ta} SuccEpsAttachRate > 1, set MM.AttEpsAttach._Ta factor to {fact}, SuccEpsAttachRate will be {r}, tried {times} times".format(ta=ta, fact=factorsDict[ta][0]["MM.AttEpsAttach._Ta"], r=SuccEpsAttachRate_New,times=SuccEpsAttachRate_TryTimes))
                #logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            else:
                pass
            # PagingSuccRate
            if PagingSuccRate >= 0.985:
                factorsDict[ta][0]["MM.FirstPagingSucc._Ta"] = 1.000
                factorsDict[ta][0]["MM.PagAtt._Ta"] = 1.000
                logger.info("{ta} PagingSuccRate already >= 0.985, set MM.FirstPagingSucc._Ta and MM.PagAtt._Ta factor to 1".format(ta=ta))
                PagingSuccRate_New = PagingSuccRate
            elif PagingSuccRate_New < 0:
                factorsDict[ta][0]["MM.FirstPagingSucc._Ta"] = 1.000
                factorsDict[ta][0]["MM.PagAtt._Ta"] = 1.000
                logger.info("{ta} PagingSuccRate exception, set MM.FirstPagingSucc._Ta and MM.PagAtt._Ta factor to 1".format(ta=ta))
                PagingSuccRate_New = PagingSuccRate
            elif PagingSuccRate_New > 1:
                PagingSuccRate_Target = random.uniform(PagingSuccRate_Min, PagingSuccRate_Max)
                PagingSuccRate_TryTimes = 0
                while(PagingSuccRate_New >=PagingSuccRate_Target):
                    PagingSuccRate_TryTimes = PagingSuccRate_TryTimes + 1
                    factorsDict[ta][0]["MM.PagAtt._Ta"] = factorsDict[ta][0]["MM.PagAtt._Ta"] + 0.001
                    PagingSuccRate_New = float((sumList(data["MM.FirstPagingSucc._Ta"], factorsDict[ta][0]["MM.FirstPagingSucc._Ta"]) + sumList(data["MM.SecondPagingSucc._Ta"]))) / float(sumList(data["MM.PagAtt._Ta"], factorsDict[ta][0]["MM.PagAtt._Ta"]))
                    #logger.info("ajust times: {times}, MM.PagAtt._Ta factor adjust to {f}, PagingSuccRate will be {r}".format(times=PagingSuccRate_TryTimes, f=factorsDict[ta][0]["MM.PagAtt._Ta"], r=PagingSuccRate_New))
                logger.info("{ta} PagingSuccRate > 1, set MM.FirstPagingSucc._Ta factor to {fact}, PagingSuccRate will be {r}, tried {times} times".format(ta=ta, fact=factorsDict[ta][0]["MM.PagAtt._Ta"], r=PagingSuccRate_New, times=PagingSuccRate_TryTimes))
                #logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            else:
                pass
            # TauUpdateRate
            if TauUpdateRate >= 0.985:
                factorsDict[ta][0]["MM.TauAccept._Ta"] = 1
                logger.info("{ta} TauUpdateRate already >= 0.985, set MM.TauAccept._Ta factor to 1".format(ta=ta))
                TauUpdateRate_New = TauUpdateRate
            elif  TauUpdateRate_New < 0:
                factorsDict[ta][0]["MM.TauAccept._Ta"] = 1
                logger.info("{ta} TauUpdateRate exception, set MM.TauAccept._Ta factor to 1".format(ta=ta))
                TauUpdateRate_New = TauUpdateRate
            elif TauUpdateRate_New > 1:
                TauUpdateRate_Target = random.uniform(TauUpdateRate_Min, TauUpdateRate_Max)
                TauUpdateRate_TryTimes = 0
                while(TauUpdateRate_New >= TauUpdateRate_Target):
                    TauUpdateRate_TryTimes = TauUpdateRate_TryTimes + 1
                    factorsDict[ta][0]["MM.TauAccept._Ta"] = factorsDict[ta][0]["MM.TauAccept._Ta"] - 0.001
                    TauUpdateRate_New = float(sumList(data["MM.TauAccept._Ta"],factorsDict[ta][0]["MM.TauAccept._Ta"])) / float(sumList(data["MM.TauRequest._Ta"]))
                logger.info("{ta} TauUpdateRate > 1, set MM.TauAccept._Ta factor to {fact}, TauUpdateRate will be {r}, tried {times} times".format(ta=ta, fact=factorsDict[ta][0]["MM.TauAccept._Ta"], r=TauUpdateRate_New,times=TauUpdateRate_TryTimes))
            else:
                pass
            logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))

             # Modify
            for counter in counters:
                for i in range(ObjectNum):
                    # Object
                    ObjectLoc = i
                    UserLabel = root[MeasurementsLoc][PmDataLoc][ObjectLoc].get("UserLabel")
                    for j in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc])):
                        if root[MeasurementsLoc][PmDataLoc][ObjectLoc][j].get("i") == counterIndexMappingDict[counter]:
                            CVLoc = j
                            if counter in counterCVNFirstHalfMappingDict:
                                counterWithTAName = counterCVNFirstHalfMappingDict[counter] + ta
                                for k in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc])-1):
                                    SNSVLoc = k
                                    SN = root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc].text
                                    if SN == counterWithTAName:
                                        SV = float(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc+1].text)
                                        fact = float(factorsDict[ta][0][counter])
                                        SV_new = int(SV * fact)
                                        root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc+1].text = str(SV_new)
                                        logger.info("{mme} {sn} {value}*{fact} -> {value_new}".format(mme=UserLabel, sn=SN, value=SV, fact=fact, value_new=SV_new))
                                    else:
                                        pass
                            else:
                                logger.error("{counter} Not in CV N First Half Mapping Directory".format(counter=counter))
                        else:
                            pass

        newFile = file.replace(".gz",".new")
        tree.write(newFile, encoding="UTF-8")
        logger.info("write {file}".format(file=newFile))
        with open(newFile, "r") as f:
            content = f.readlines()
        for i in range(len(content)):
            if content[i].startswith("<PmFile"):
                content[i] = "<PmFile xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
                logger.info("modified name space information")
                break
        with open(newFile, "w") as f:
            f.writelines(content)
        logger.info("generated {file}".format(file=newFile))

        with open(newFile, "r") as f:
            content = f.read()
        filename = os.path.basename(file)
        curdir = os.getcwd()
        finalOutDir = os.path.join(globVar.OUTPUT_DIR,file.split(os.path.sep)[-2])
        if not os.path.isdir(finalOutDir):
            os.mkdir(finalOutDir)
            logger.info("create directory {dir}".format(dir=finalOutDir))
        os.chdir(finalOutDir)
        fh = gzip.open(filename, 'w')
        try:
            fh.write(content)
            logger.info("generated {file}".format(file=os.path.join(finalOutDir, filename)))
        finally:
            os.chdir(curdir)
            fh.close()
        # end process

        # update in processed.json
        processedJsonTime = fileName.split("-")[4][:10]
        processedFile = os.path.join(globVar.LOG_DIR, processedJsonTime + ".processed.json")
        if not os.path.isfile(processedFile):
            list = []
            list.append(fileName)
            with open(processedFile, "w") as f:
                json.dump(list, f, indent=1)
        else:
            with open(processedFile, "r") as f:
                list = json.load(f)
            list.append(fileName)
            with open(processedFile, "w") as f:
                json.dump(list,f, indent=1)
        logger.info("updated {fileName} in processed json {pfile}".format(fileName=fileName, pfile=processedFile))

    logger.info("#--- Exit Phase 2: Process MME files in repo ---#")

def stopBanner():
    logger.info("STOP")

if __name__ == "__main__":
    startBanner()
    checkProcess()
    scanAndCopy()
    process()
    stopBanner()