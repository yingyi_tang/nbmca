import globVar
import commands
import logging
import logging.config
import os

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("monitor")
except Exception, e:
    print(e)

def housekeeping():
    try:
        cmdList = [
            "/bin/find {csvDir}/ -mtime +8 -type f -exec rm {char} \;".format(csvDir=globVar.CSV_DIR, char="{}"), \
            "/bin/find {logDir}/*.json -mtime +3 -type f -exec rm {char} \;".format(logDir=globVar.LOG_DIR, char="{}"), \
            "/bin/find {tmpDir}/ -mtime +3 -type f -exec rm {char} \;".format(tmpDir=globVar.TMP_DIR, char="{}"),
            "/bin/find {workDir}/ -mtime +8 -type d -exec rm -rf {char} \;".format(workDir=globVar.WORK_DIR, char="{}")
        ]
        for cmd in cmdList:
            status, ret = commands.getstatusoutput(cmd)
            if status==0:
                logger.info("run {cmd}".format(cmd=cmd))
            else:
                logger.error("run {cmd} failed, return {ret}".format(cmd=cmd, ret=ret))

        # M
        fsize = os.path.getsize(globVar.LOG_FILE)/1024/2014
        if fsize >= 50:
            os.remove(globVar.LOG_FILE)
            logger.info("log file size {size}M, remove {file}".format(size=fsize, file=globVar.LOG_FILE))
        else:
            logger.info("log file size {size}M, no need to delete".format(size=fsize))
    except Exception, e:
        logger.error(e)

if __name__ == "__main__":
    housekeeping()
