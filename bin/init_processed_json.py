import datetime
import globVar
import commands
import os
import logging
import logging.config
import subprocess
import json
import shutil

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)

now = datetime.datetime.now()

def init():
    PERIOD = -6
    i = 0

    newlyAddedFilesList = []
    while i > PERIOD:
        processedList = []  # Read from <date>.processed.json
        delta = datetime.timedelta(hours=i)
        folderTime = (now + delta).strftime("%Y%m%d%H")
        dir = os.path.join(globVar.OUTPUT_DIR, folderTime)   # locate to different input folder by input/YYMMDDhh

        logger.info("checking {dir}".format(dir=dir))
        cmd = "ls -th {dir} | grep PM-MME | grep gz".format(dir=dir)   # find all PM files under input/YYMMDDhh
        status, output = commands.getstatusoutput(cmd)
        logger.info(cmd)
        if status == 0:
            # Get all MME PM files name list - pmFilesNamelist
            pmFilesNamelist = output.split("\n")
            logger.info(pmFilesNamelist)
            logger.info("found total {num} MME PM files".format(num=len(pmFilesNamelist)))

            #  Get Processed PM files name list - processedFile
            processedFile = os.path.join(globVar.LOG_DIR, folderTime+".processed.json")
            if os.path.isfile(processedFile):
                with open(processedFile, "r") as f:
                    processedList = json.load(f)
                logger.info("read processed json file: {file}".format(file = processedFile))
                #logger.info("processed files list: {list}".format(list=processedList))
                newlyAddedFilesList = [ x for x in pmFilesNamelist if x not in processedList ]
                logger.info("newly added {num} files list: {list}".format(num=len(newlyAddedFilesList), list=newlyAddedFilesList))
            else:
                logger.info("processed json file is not exist")
                newlyAddedFilesList = pmFilesNamelist
                logger.info("newly added {num} files list: {list}".format(num=len(newlyAddedFilesList), list=newlyAddedFilesList))
            with open(processedFile, "w") as f:
                json.dump(newlyAddedFilesList, f, indent=1)
            logger.info("generated {fileName}".format(fileName=processedFile))
        else:
            logger.warning("code: {status}, output: {output}".format(status=status, output=output))

        i = i - 1
    logger.info("process files list: {files}".format(files=newlyAddedFilesList))

if __name__ == "__main__":
    init()