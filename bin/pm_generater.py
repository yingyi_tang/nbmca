"""
Usage
python /opt/nbi/DataFile/nbmca/bin/pm_generater.py 60 RN
python /opt/nbi/DataFile/nbmca/bin/pm_generater.py 90 RY
"""

import globVar
import sys
import datetime
import os
import time
import shutil
"""
<nbi@gzltenbm01> </opt/nbi/DataFile/GD/HX/ER/GZ_OMC1/PM/2019021209> ls -lrth | grep MME | grep -v R1
-rw-r--r-- 1 nbi nbi 1.4M Feb 12 09:26 PM-MME-A1-V3.0.0-20190212090000-15.xml.gz
-rw-r--r-- 1 nbi nbi 1.4M Feb 12 09:41 PM-MME-A1-V3.0.0-20190212091500-15.xml.gz
-rw-r--r-- 1 nbi nbi 1.4M Feb 12 09:56 PM-MME-A1-V3.0.0-20190212093000-15.xml.gz
-rw-r--r-- 1 nbi nbi 1.4M Feb 12 10:11 PM-MME-A1-V3.0.0-20190212094500-15.xml.gz
"""

def pm_generater():
    neType = ["MME","HSS"]

    try:
        period = sys.argv[1]
        rFlag = sys.argv[2]
    except Exception,e:
        print(e)
        exit(1)

    while(True):
        now = datetime.datetime.now()
        currentDate = now.strftime("%Y%m%d")
        currentTime = now.strftime("%H%M") + "00"
        currentFolderName = now.strftime("%Y%m%d%H")

        inputDir = os.path.join(globVar.OUTPUT_DIR, currentFolderName)
        if not os.path.isdir(inputDir):
            os.mkdir(inputDir)

        for i in range(len(neType)):
            if rFlag == "RY":
                filename = "PM-" + neType[i] + "-A1-V3.0.0-" + currentDate + currentTime + "-R1-15.xml.gz"
            else:
                filename = "PM-" + neType[i] + "-A1-V3.0.0-" + currentDate + currentTime + "-15.xml.gz"
            file = os.path.join(inputDir, filename)
            shutil.copy(os.path.join(globVar.SAMPLE_DIR,"PM-MME-A1-V3.0.0-20190202121500-15.xml.gz"), file)
        time.sleep(float(period))

if __name__ == "__main__":
    pm_generater()

